from JerkBotSpawner import JerkBotSpawner
from twisted.internet import defer, protocol, reactor, ssl
from twisted.words.protocols import irc
import urllib2
from bs4 import BeautifulSoup
import GlobalSettings

class JerkBot(irc.IRCClient):
    nickname = 'JerkCityBot'
    password = GlobalSettings.PASSWORD

    def __init__(self):
        self.deferred = defer.Deferred()

    def connectionLost(self, reason):
        self.deferred.errback(reason)

    def signedOn(self):
        for channel in self.factory.channels:
            self.join(channel)

    def privmsg(self, user, channel, message):
        nick, _, host = user.partition('!')
        message = message.strip()
        if not message.startswith('!'):
            return  # so do nothing
        command, sep, rest = message.lstrip('!').partition(' ')

        func = getattr(self, 'command_' + command, None)

        if func is None:
            return

        d = defer.maybeDeferred(func, rest)

        d.addErrback(self._showError)

        if channel == self.nickname:
            d.addCallback(self._sendMessage, nick)
        else:
            d.addCallback(self._sendMessage, channel, nick)

    def _sendMessage(self, msg, target, nick=None):
        if msg is None:
            return
        if nick:
            msg = '%s' % (msg)
        self.msg(target, msg)

    def _showError(self, failure):
        return failure.getErrorMessage()

    def command_ping(self, rest):
        return 'Pong.'

    def command_jerks(self, rest):
        if rest == '' or rest.isspace():
            response = urllib2.urlopen('http://www.jerkcity.com/random/?_')
            print("URL IS" + response.geturl())

            data = response.read()
            soup = BeautifulSoup(data, "html.parser")
            text = soup.find("textarea", {"name": "dialogedit"})
            comicText = text.get_text()
            self.ParseJerkCityComicText(comicText)
        else:
            response = urllib2.urlopen('http://www.jerkcity.com/search/?q=%s' % urllib2.quote(rest))
            data = response.read()
            soup = BeautifulSoup(data, "html.parser")

            if soup.find_all('a'):
                url = soup.find_all('a')[0].get('href')
                fullUrl =  "http://www.jerkcity.com%s" % url
                response = urllib2.urlopen( fullUrl)
                print("URL FOR COMIC IS " + response.geturl())
                data = response.read()
                soup = BeautifulSoup(data, "html.parser")
                text = soup.find("textarea", {"name": "dialogedit"})
                comicText = text.get_text()
                self.ParseJerkCityComicText(comicText)



    def ParseJerkCityComicText(self, comicText):
        jerkBotSpawner = JerkBotSpawner(comicText)
        jerkBotSpawner.sendComicToChat()



class JerkBotIRCFactory(protocol.ReconnectingClientFactory):
    protocol = JerkBot
    channels = [GlobalSettings.CHANNEL]


def main():
    f = JerkBotIRCFactory()
    reactor.connectSSL(GlobalSettings.SERVER, GlobalSettings.PORT, f, ssl.ClientContextFactory())
    reactor.run()

if __name__ == '__main__':
    main()